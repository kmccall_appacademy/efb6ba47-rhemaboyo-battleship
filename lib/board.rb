require 'byebug'
class Board
  attr_accessor :grid, :bank

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
    @bank = (0...grid.length).reduce([]) do |arr, el|
      i = 0
      grid.length.times do
        arr << [el, i]
        i += 1
      end
      arr
    end
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end

  def count
    grid.flatten.count(:s)
  end

  def won?
    grid.flatten.none? { |el| el == :s }
  end

  def empty?(pos = nil)
    if pos
      self[pos] == nil
    else
      grid.flatten.all? { |el| el == nil }
    end
  end

  def full?
    grid.flatten.none? { |el| el == nil }
  end

  def place_random_ship
    if full?
      raise "Board is full"
    else
      pos = bank.delete_at((0...bank.length).to_a.sample)
      self[pos] = :s if self[pos] == nil
    end
  end

end
